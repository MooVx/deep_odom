import pickle
import numpy as np
import torch.utils.data
import os
import copy
import math
import random
SCALE_ODOM = 10.0  # Odometries in [dm] - prevent loss to be extremely low values.

from functions import printProgressBar, rotate


def default_sample_loader(path, sequence):
    dataset = dict()
    images = list()
    odometries = list()

    path_to_pickle = path + 'dataset_' + sequence + '.pickle'
    # print("Data set pickle loading: Path: ", path_to_pickle)

    if os.path.isfile(path_to_pickle):
        with open(path_to_pickle, 'rb') as handle:
            dataset = pickle.load(handle)
            for i in range(len(dataset['img'])):
                x = dataset['north'][i] * SCALE_ODOM
                y = dataset['east'][i] * SCALE_ODOM
                yaw = np.deg2rad(dataset['yaw'][i] - dataset['yaw'][0]) * SCALE_ODOM
                odom = np.array([x, y, yaw]).astype(np.float32)
                odometries.append(odom)
                # normalize image values to range < 0, 1 > - original image is unsigned int 16-bit grayscale image
                images.append(np.array(dataset['img'][i]).astype(np.float32) / (2.0**8-1))

    return odometries, images


class VisualOdometryDataLoader(torch.utils.data.Dataset):
    def __init__(self, datapath, trajectory_length=1, augmentation=None, loader=default_sample_loader):
        self.base_path = datapath
        self.augmentation = augmentation
        self.loader = loader
        self.trajectory_length = trajectory_length
        self.max_idx_diff = 3
        self.current_sequence = 0
        self.sequences = list()

        for file in os.listdir(self.base_path):
            if file.endswith(".pickle"):
                seq = file[len(file) - 10:len(file) - 7]
                self.sequences.append(seq)
        random.shuffle(self.sequences)
        path_to_print = copy.deepcopy(self.base_path)
        while len(path_to_print) < 60:
            path_to_print = path_to_print + " "
        print("Number of sequences in: " + path_to_print + "    is: ", len(self.sequences))

        self.dataset_index = 0
        self.poses = list()
        self.images = list()

    def __getitem__(self, index):
        images_stacked = list()
        odometries = list()

        # Try to load new data if it is needed
        dataset_valid = False
        while not dataset_valid:
            if len(self.poses) == 0:
                # load the data if current data set is empty
                # print("Data empty, loading.")
                if len(self.poses) == 0 and self.current_sequence == 0:
                    # random.shuffle(self.sequences)
                    # print("Shuffling list: ", self.sequences[0:5])
                    random.shuffle(self.sequences)
                    # print("Shuffling list: ", self.sequences[0:5])
                if (self.current_sequence) < len(self.sequences):
                    self.dataset_index = 0
                    self.poses, self.images = self.loader(self.base_path, self.sequences[self.current_sequence])
                    if self.trajectory_length + self.dataset_index + 1 > len(self.poses):
                        dataset_valid = True
            elif self.trajectory_length + self.dataset_index + 1 >= len(self.poses):
                # load new data set if current is fully explored
                # print("Sequence explored, loading.")
                self.current_sequence = self.current_sequence + 1
                if (self.current_sequence) < len(self.sequences):
                    self.dataset_index = 0
                    self.poses, self.images = self.loader(self.base_path, self.sequences[self.current_sequence])
                    if len(self.poses) != 0:
                        dataset_valid = True
            else:
                # print("Sequence still valid, waiting. Index: ", self.dataset_index)
                # previous data set is still valid
                dataset_valid = True

        # print("Data set valid: ", dataset_valid)
        if dataset_valid:
            if self.augmentation:
                idx_list = np.zeros(self.trajectory_length, np.int32)
                if self.dataset_index + self.trajectory_length * self.max_idx_diff + 1 < len(self.poses):
                    idx_list[0] = np.int(np.random.uniform(1.0, np.double(self.max_idx_diff) + 0.999))
                    for i in range(1, self.trajectory_length):
                        rand_idx = np.int(np.random.uniform(0.0, np.double(self.max_idx_diff) + 0.999))
                        idx_list[i] = rand_idx + idx_list[i - 1]
                    idx_list += self.dataset_index
                    # print("Index list: ", idx_list)
                else:
                    current_max_diff = np.int(((len(self.poses)-2) - self.dataset_index) / self.trajectory_length)
                    # print("Current max diff: ", current_max_diff)
                    idx_list[0] = np.int(np.random.uniform(1.0, np.double(current_max_diff) + 0.999))
                    for i in range(1, self.trajectory_length):
                        rand_idx = np.int(np.random.uniform(0.0, np.double(current_max_diff) + 0.999))
                        idx_list[i] = rand_idx + idx_list[i - 1]
                    idx_list += self.dataset_index
                    # print("Index list: ", idx_list)

                for i in range(self.trajectory_length):
                    # print("Read samples from: ", idx_list[i], "  to: ", idx_list[i]+1)

                    img1 = self.images[idx_list[i]]
                    img2 = self.images[idx_list[i]+1]
                    pose1 = self.poses[idx_list[i]]
                    pose2 = self.poses[idx_list[i]+1]

                    # -------------------------------------------------------------------------------------------------
                    # New version with local coordinates
                    odom = np.zeros_like(pose1, np.float64)
                    # extract X,Y global coordinates
                    point_1 = pose1[0:2]
                    point_2 = pose2[0:2]
                    # get rotation angle - rotate pose2 with pose1 angle (pose1 - location of local coordinates system)
                    rot_angle = (-1.0) * pose1[2] / SCALE_ODOM

                    # calculate translation in global coordinates system
                    global_detla = point_2 - point_1
                    # rotate translation vector - after this we will get translation in local coordinate system
                    local_delta = np.array(rotate([0, 0], global_detla, rot_angle))

                    # copy result to odom vector
                    odom_x, odom_y = local_delta
                    odom_th = pose2[2] - pose1[2]
                    odom[0] = odom_x
                    odom[1] = odom_y
                    odom[2] = odom_th

                    # -------------------------------------------------------------------------------------------------
                    # Old version with global coordinates
                    # odom = pose2 - pose1
                    # -------------------------------------------------------------------------------------------------
                    odometries.append(odom)

                    img1 = img1.reshape(720, 1280, 1)
                    img2 = img2.reshape(720, 1280, 1)
                    image = np.concatenate([img1, img2], axis=2)
                    image = np.transpose(image, [2, 0, 1])
                    images_stacked.append(image)

                self.dataset_index = self.dataset_index + self.trajectory_length

            else:
                for i in range(self.dataset_index, self.dataset_index + self.trajectory_length):
                    img1 = self.images[self.dataset_index]
                    img2 = self.images[self.dataset_index+1]
                    pose1 = self.poses[self.dataset_index]
                    pose2 = self.poses[self.dataset_index+1]

                    # -------------------------------------------------------------------------------------------------
                    # New version with local coordinates
                    odom = np.zeros_like(pose1, np.float64)
                    # extract X,Y global coordinates
                    point_1 = pose1[0:2]
                    point_2 = pose2[0:2]
                    # get rotation angle - rotate pose2 with pose1 angle (pose1 - location of local coordinates system)
                    rot_angle = (-1.0) * pose1[2] / SCALE_ODOM

                    # calculate translation in global coordinates system
                    global_detla = point_2 - point_1
                    # rotate translation vector - after this we will get translation in local coordinate system
                    local_delta = np.array(rotate([0, 0], global_detla, rot_angle))

                    # copy result to odom vector
                    odom_x, odom_y = local_delta
                    odom_th = pose2[2] - pose1[2]
                    odom[0] = odom_x
                    odom[1] = odom_y
                    odom[2] = odom_th

                    # -------------------------------------------------------------------------------------------------
                    # Old version with global coordinates
                    # odom = pose2 - pose1
                    # -------------------------------------------------------------------------------------------------
                    odometries.append(odom)

                    img1 = img1.reshape(720, 1280, 1)
                    img2 = img2.reshape(720, 1280, 1)
                    image = np.concatenate([img1, img2], axis=2)
                    image = np.transpose(image, [2, 0, 1])
                    images_stacked.append(image)

                # print("Readed samples from: ", self.dataset_index)
                self.dataset_index = self.dataset_index + self.trajectory_length

        else:
            print("Data set fully explored!")
        # print(odometries)
        return np.asarray(images_stacked), np.asarray(odometries)

    def get_total_samples(self, path, sequences):
        total_size = 0
        for i in range(len(sequences)):
            path_to_pickle = path + 'dataset_' + sequences[i] + '.pickle'
            if os.path.isfile(path_to_pickle):
                total_size = total_size + np.int(199/self.trajectory_length)
        # print("Total size: ", total_size)
        return total_size 

    def __len__(self):
        # print("Total samples: ", np.int(self.get_total_samples(self.base_path, self.sequences) / self.trajectory_length))
        return np.int(self.get_total_samples(self.base_path, self.sequences))
