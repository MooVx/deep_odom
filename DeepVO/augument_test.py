import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
plt.switch_backend('TkAgg')

dataset_index = 0
trajectory_length = 6
max_idx_diff = 4
sample_length = 200

desired_samples = np.int(sample_length / trajectory_length)

idx_list = np.zeros(trajectory_length, np.int32)
while dataset_index < sample_length - trajectory_length:
    if dataset_index + trajectory_length*max_idx_diff + 1 < sample_length:
        idx_list[0] = np.int(np.random.uniform(1.0, np.double(max_idx_diff) + 0.999))
        for i in range(1, trajectory_length):
            rand_idx = np.int(np.random.uniform(1.0, np.double(max_idx_diff) + 0.999))
            idx_list[i] = rand_idx + idx_list[i-1]
        idx_list += dataset_index
        print(idx_list)
    else:
        current_max_diff = np.int((sample_length - dataset_index) / trajectory_length)
        print("Current_max_diff: ", current_max_diff)
        idx_list[0] = np.int(np.random.uniform(1.0, np.double(current_max_diff) + 0.999))
        for i in range(1, trajectory_length):
            rand_idx = np.int(np.random.uniform(1.0, np.double(current_max_diff) + 0.999))
            idx_list[i] = rand_idx + idx_list[i-1]
        idx_list += dataset_index
        print(idx_list)

    dataset_index = dataset_index + trajectory_length

print('Desired samples: ', desired_samples)
