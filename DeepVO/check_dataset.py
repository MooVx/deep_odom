import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
import math
# plt.switch_backend('TkAgg')


def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.
    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point
    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy


# path_to_pickle = '/media/roman/Garry/DeepVO_Dataset_RGB/Train/dataset_031.pickle'
# print("Data set pickle loading: Path: ", path_to_pickle)

# images = list()
# raw_odometries = list()
# dataset = dict()

# if os.path.isfile(path_to_pickle):
#     with open(path_to_pickle, 'rb') as handle:
#         dataset = pickle.load(handle)
#         for i in range(len(dataset['img'])):
#             x = dataset['north'][i]
#             y = dataset['east'][i]
#             yaw = np.deg2rad(dataset['yaw'][i])
#             odom = np.array([x, y, yaw]).astype(np.float32)
#             raw_odometries.append(odom)
#             # normalize image values to range < 0, 1 > - original image is unsigned int 16-bit grayscale image
#             images.append(np.array(dataset['img'][i]).astype(np.float32) / (2.0 ** 8 - 1))

#             # plt.ion()
#             # plt.figure(1, figsize=[16, 9])
#             #
#             # plt.imshow(images[i], cmap='gray')
#             # plt.title("Sample image nr: = " + str(i))
#             # plt.draw()
#             # plt.pause(0.033)
#             # plt.clf()
#             #
#             # plt.ioff()


# raw_odometries = np.array(raw_odometries)
# raw_x = raw_odometries[:, 0]
# raw_y = raw_odometries[:, 1]
# raw_yaw = raw_odometries[:, 2] - raw_odometries[0, 2]

# dx = np.zeros((len(raw_x)-1), np.float64)
# dy = np.zeros((len(raw_x)-1), np.float64)
# dth = np.zeros((len(raw_x)-1), np.float64)
# for i in range(len(raw_x)-1):
#     point_1 = np.array((raw_x[i], raw_y[i]))
#     point_2 = np.array((raw_x[i+1], raw_y[i+1]))
#     angle = raw_yaw[i]

#     global_detla = point_2 - point_1
#     print("global_detla: ", global_detla)
#     print("Angle:        ", np.rad2deg(angle))
#     local_delta = np.array(rotate([0, 0], global_detla, -angle))
#     print("local delta:  ", local_delta)

#     odom_x, odom_y = local_delta
#     odom_th = raw_yaw[i+1] - raw_yaw[i]

#     dx[i] = odom_x
#     dy[i] = odom_y
#     dth[i] = odom_th


# reversed_x = np.zeros((len(raw_x)), np.float64)
# reversed_y = np.zeros((len(raw_x)), np.float64)
# reversed_yaw = np.zeros((len(raw_x)), np.float64)
# system_coords = np.array([0, 0, 0])
# for i in range(len(raw_x) - 1):
#     local_delta = np.array([dx[i], dy[i]])
#     global_detla = rotate([0, 0], local_delta, system_coords[2])

#     reversed_x[i+1] = system_coords[0] + global_detla[0]
#     reversed_y[i+1] = system_coords[1] + global_detla[1]
#     reversed_yaw[i+1] = system_coords[2] + dth[i]
#     system_coords = np.array([reversed_x[i+1], reversed_y[i+1], reversed_yaw[i+1]])


# plt.figure(1)
# plt.scatter(raw_odometries[:, 0], raw_odometries[:, 1], s=0.3)
# plt.title('Global trajectory')

# plt.figure(2)
# plt.scatter(np.arange(0, len(raw_odometries[:, 0])), raw_odometries[:, 0], s=0.3)
# plt.scatter(np.arange(0, len(raw_odometries[:, 0])), raw_odometries[:, 1], s=0.3)
# plt.legend(['X', 'Y'])
# plt.title('Global X,Y trajectories')

# plt.figure(3)
# plt.plot(dx)
# plt.plot(dy)
# plt.plot(dth)
# plt.legend(['X', 'Y', 'YAW'])
# plt.title('Local increments')

# plt.figure(4)
# plt.scatter(reversed_x, reversed_y, s=0.3)
# plt.title('Reversed trajectory')
# plt.show()

if __name__ == "__main__":
    path = "/home/eve/DeepVO_Dataset_RGB/Checkpoint/checkpoint_58_3_estimated_odometry.pickle"
    with open(path,'rb') as file :
        odom = pickle.load(file)
    # network results
    odom = np.asarray(odom)
    dx = odom[:,0]/1.0
    dy = odom[:,1]/1.0
    dth = odom[:,2]/1.0
    reversed_x = np.zeros((len(dx)), np.float64)
    reversed_y = np.zeros((len(dx)), np.float64)
    reversed_yaw = np.zeros((len(dx)), np.float64)
    system_coords = np.array([0, 0, 0])
    for i in range(len(dx) - 1):
        local_delta = np.array([dx[i], dy[i]])
        global_detla = rotate([0, 0], local_delta, system_coords[2])

        reversed_x[i+1] = system_coords[0] + global_detla[0]
        reversed_y[i+1] = system_coords[1] + global_detla[1]
        reversed_yaw[i+1] = system_coords[2] + dth[i]
        system_coords = np.array([reversed_x[i+1], reversed_y[i+1], reversed_yaw[i+1]])
    plt.figure()
    plt.scatter(reversed_x, reversed_y, s=0.3)
    plt.title('Reversed trajectory')
    plt.savefig('test.png', dpi=400)
    # plt.show()