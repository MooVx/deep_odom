import torch
import torch.nn as nn
from torch.autograd import Variable

SIZE = 1000

class DeepOdomNet(nn.Module):
    print_shape = True
    def __init__(self, trajectory_length):
        super(DeepOdomNet, self).__init__()
        print("Init DeepVoNew model!")
        self.conv1 = nn.Conv2d(2, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3))

        self.relu1 = nn.LeakyReLU(inplace=True)
        self.maxpool1 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv2 = nn.Conv2d(64, 128, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2))
        self.relu2 = nn.LeakyReLU(inplace=True)
        self.maxpool2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.conv3 = nn.Conv2d(128, 256, kernel_size=(5, 5), stride=(2, 2), padding=(2, 2))
        self.relu3 = nn.LeakyReLU(inplace=True)

        self.conv3_1 = nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.relu3_1 = nn.LeakyReLU(inplace=True)

        self.conv4 = nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1))
        self.relu4 = nn.LeakyReLU(inplace=True)

        self.conv4_1 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.relu4_1 = nn.LeakyReLU(inplace=True)

        self.conv5 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1))
        self.relu5 = nn.LeakyReLU(inplace=True)

        self.conv5_1 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.relu5_1 = nn.LeakyReLU(inplace=True)

        self.conv6 = nn.Conv2d(512, 1024, kernel_size=(3, 3), stride=(2, 2), padding=(1, 1))

        self.lstm1 = nn.LSTMCell(5*3*1024, SIZE)
        self.lstm2 = nn.LSTMCell(SIZE, SIZE)
        self.fc = nn.Linear(in_features=SIZE, out_features=3)

        self.reset_hidden_states(trajectory_length, True)

    def reset_hidden_states(self, size=1, zero=True):
        if zero == True:
            self.hx1 = Variable(torch.zeros(size, SIZE))
            self.cx1 = Variable(torch.zeros(size, SIZE))
            self.hx2 = Variable(torch.zeros(size, SIZE))
            self.cx2 = Variable(torch.zeros(size, SIZE))
        else:
            self.hx1 = Variable(self.hx1.data)
            self.cx1 = Variable(self.cx1.data)
            self.hx2 = Variable(self.hx2.data)
            self.cx2 = Variable(self.cx2.data)

        if torch.cuda.is_available():
            self.hx1 = self.hx1.cuda()
            self.cx1 = self.cx1.cuda()
            self.hx2 = self.hx2.cuda()
            self.cx2 = self.cx2.cuda()

    def reset_network_weights(self):
        self.conv1.reset_parameters()
        self.conv2.reset_parameters()
        self.conv3.reset_parameters()
        self.conv3_1.reset_parameters()
        self.conv4.reset_parameters()
        self.conv4_1.reset_parameters()
        self.conv5.reset_parameters()
        self.conv5_1.reset_parameters()
        self.conv6.reset_parameters()
        self.lstm1.reset_parameters()
        self.lstm2.reset_parameters()
        self.fc.reset_parameters()

    def forward(self, x):        
        if self.print_shape:
            print('\n\n---------------------------------------------------------')
            print("              Network layers - Sequential\n")
            print("Input         -     ", x.shape)
        x = self.conv1(x)
        x = self.relu1(x)
        if self.print_shape:
            print("Conv1         -     ", x.shape)
        x = self.maxpool1(x)
        if self.print_shape:
            print("MaxPool1      -     ", x.shape)
        x = self.conv2(x)
        x = self.relu2(x)
        if self.print_shape:
            print("Conv2         -     ", x.shape)
        x = self.maxpool2(x)
        if self.print_shape:
            print("MaxPool2      -     ", x.shape)
        x = self.conv3(x)
        x = self.relu3(x)
        if self.print_shape:
            print("Conv3         -     ", x.shape)
        x = self.conv3_1(x)
        x = self.relu3_1(x)
        if self.print_shape:
            print("Conv3_1       -     ", x.shape)
        x = self.conv4(x)
        x = self.relu4(x)
        if self.print_shape:
            print("Conv4         -     ", x.shape)
        x = self.conv4_1(x)
        x = self.relu4_1(x)
        if self.print_shape:
            print("Conv4_1       -     ", x.shape)
        x = self.conv5(x)
        x = self.relu5(x)
        if self.print_shape:
            print("Conv5         -     ", x.shape)
        x = self.conv5_1(x)
        x = self.relu5_1(x)
        if self.print_shape:
            print("Conv5_1       -     ", x.shape)
        x = self.conv6(x)
        if self.print_shape:
            print("Conv6         -     ", x.shape)
        x = x.view(x.size(0), 5 * 3 * 1024)
        if self.print_shape:
            print("View          -     ", x.shape)
        self.hx1, self.cx1 = self.lstm1(x, (self.hx1, self.cx1))
        x = self.hx1
        if self.print_shape:
            print("LSTM1         -     ", x.shape)
        self.hx2, self.cx2 = self.lstm2(x, (self.hx2, self.cx2))
        x = self.hx2
        if self.print_shape:
            print("LSTM2         -     ", x.shape)
        x = self.fc(x)
        if self.print_shape:
            print("Linear Out    -     ", x.shape)
            print('\n---------------------------------------------------------\n\n')
            self.print_shape = False
        return x
