import torch
import torch.nn as nn
from torch.autograd import Variable
SIZE = 1000


def conv(in_planes, out_planes, kernel_size=3, stride=1, bias=False):
    seq =  nn.Sequential(
        nn.Conv2d(in_planes, out_planes, kernel_size, stride,  bias=False),
        nn.BatchNorm2d(out_planes),
        nn.LeakyReLU(0.1, inplace=True)
    )
    # seq[0].requires_grad_(False)
    return seq

class DeepOdomNet(nn.Module):
    layer_list = []

    def __init__(self):
        super(DeepOdomNet, self).__init__()
        print("Init DeepOdomNet model!")

        self.layers = nn.Sequential(
            conv(2, 64, kernel_size=8, stride=3),
            conv(64, 128, kernel_size=8),
            conv(128, 128, kernel_size=5, stride=3),
            nn.MaxPool2d(kernel_size=2),
            conv(128, 128, kernel_size=5),
            conv(128, 256, kernel_size=3),
            nn.Flatten(),
            nn.Linear(in_features=516096, out_features=3, bias=False)
            )

    def forward(self, x):
        return self.layers(x)

