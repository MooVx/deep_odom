from tqdm import tqdm
from eve_dataset import VisualOdometryDataLoader
from deepodomnet import DeepOdomNet
from torch.autograd import Variable
from torchsummary import summary

import torch.optim as optim
import torch
import argparse
import os
import time

import matplotlib.pyplot as plt
import numpy as np
import copy
import pickle
import math
plt.switch_backend('Agg')
import matplotlib
matplotlib.use('Agg')

from functions import printProgressBar, rotate

USE_CUDA = torch.cuda.is_available()
print("Is CUDA available: ", USE_CUDA)
FLOAT = torch.cuda.FloatTensor if USE_CUDA else torch.FloatTensor
CHUNKS = 4

SCALE_ODOM = 1.0
TRAJECTORY_LENGTH = 6


def criterion(output, target):
    K1 = 10.0
    K2 = 10.0
    K3 = 20.0
    print("\033[3A\r ")
    print(
        f"Position X Err:  {float((torch.sum((K1 *(output[:, :, 0] - target[:, :, 0]))**2))):4.4f}                      ",
        f"Position Y Err:  {float((torch.sum((K2 *(output[:, :, 1] - target[:, :, 1]))**2))):4.4f}                      ",
        f"Orientation Err: {float((torch.sum((K3 *(output[:, :, 2] - target[:, :, 2]))**2))):4.4f}                    \n")

    # loss = torch.sum((K1 * (output[:, :, 0] - target[:, :, 0]))**2)

    loss = torch.sum((K1 * (output[:, :, 0] - target[:, :, 0]))**2)\
        + torch.sum((K2 * (output[:, :, 1] - target[:, :, 1]))**2)\
        + (torch.sum((K3 * (output[:, :, 2] - target[:, :, 2]))**2))    

    # loss = (torch.sum((K3 * (output[:, :, 2] - target[:, :, 2]))**2))

    return loss


def train_model(model, train_loader, criterion, optimizer, epoch, nr_of_epochs, batch_size,  train_losses, dtype=FLOAT, used_criterion_th=0.0):

    epoch_train_losses = list()
    cnt = 0
    nr_of_batches = np.int(len(train_loader.dataset) / batch_size)
    model.train()
    model.training = True

    for batch_idx, (images_stacked, odometries_stacked) in enumerate(train_loader):
        if cnt == 0:
            batch_start_time = time.time()
        if USE_CUDA:
            images_stacked = images_stacked.cuda()
            odometries_stacked = odometries_stacked.cuda()

        images_stacked = Variable(images_stacked).type(dtype)
        odometries_stacked = Variable(odometries_stacked).type(dtype)
        estimated_odometries = Variable(
            torch.zeros(odometries_stacked.shape)).type(dtype)
        if USE_CUDA:
            estimated_odometries = estimated_odometries.cuda()

        for t in range(batch_size):
            model.reset_hidden_states(size=TRAJECTORY_LENGTH, zero=True)
            # compute output
            estimated_odometry = model(images_stacked[t])
            estimated_odometries[t] = estimated_odometry

        train_loss = criterion(estimated_odometries, odometries_stacked)

        if float(train_loss) > used_criterion_th:
            train_loss.backward()

        cnt += 1
        if cnt == CHUNKS:
            cnt = 0
            if float(train_loss) > used_criterion_th:
                optimizer.step()
                optimizer.zero_grad()

            epoch_train_losses.append(np.float(train_loss.data.cpu()))
            batch_stop_time = time.time()

            prefix = f"Epoch: {epoch:d}/{nr_of_epochs:d}"
            suffix = f" {int(batch_idx/CHUNKS) + 1:d}/{int(nr_of_batches/CHUNKS):d}" + \
                f"   Batch time: {(batch_stop_time - batch_start_time):0.2f} [s]" + \
                f"   Current train LOSS: {np.float(train_loss.data.cpu()):0.3f}" + \
                f"   Total progress: {((((int(batch_idx/CHUNKS) + 1) / (int(nr_of_batches/CHUNKS) * nr_of_epochs)) + ((epoch - 1) / nr_of_epochs)) * 100):0.3f} [%]          "
            printProgressBar(int(batch_idx/CHUNKS) + 1, int(nr_of_batches/CHUNKS),
                             prefix=prefix, suffix=suffix)

    train_losses.append(np.mean(epoch_train_losses))
    print("")
    print("Mean training LOSS for epoch: ", np.mean(epoch_train_losses))


def train(model, args):
    model.training = True
    val_losses = list()
    train_losses = list()
    # summary(model, input_size=(2, 720, 1280))

    kwargs = {'num_workers': 1, 'pin_memory': False} if torch.cuda.is_available() else {
    }
    train_loader = torch.utils.data.DataLoader(
        VisualOdometryDataLoader(
            args.train_datapath, trajectory_length=TRAJECTORY_LENGTH, augmentation=True),
        batch_size=args.gpu_batch_size, shuffle=False, drop_last=True, **kwargs)
    val_loader = torch.utils.data.DataLoader(
        VisualOdometryDataLoader(
            args.val_datapath, trajectory_length=TRAJECTORY_LENGTH, augmentation=None),
        batch_size=args.gpu_batch_size, shuffle=False, drop_last=True, **kwargs)
    print(
        f'Batches (GPU Batch Size: {args.gpu_batch_size:d} * Chunks: {CHUNKS:d} = {args.gpu_batch_size*CHUNKS:d} Virtual Batch Size.)')
    # print("\n\n\n\n\n\033[5B")
    used_criterion_th = 0.0

    optimizer = torch.optim.Adam(model.parameters(),lr=args.lr)

    for epoch in range(1,args.epoch_num + 1):
        print("----------------------------------------------------------------------------------------------------")
        print("Starting training epoch nr: ", epoch)
        # train for one epoch
        start_time = time.time()
        if epoch % 3 == 1:
            used_criterion_th = 0.0

        print("\nUsed criterion treshold: ", used_criterion_th)
        print("\n\n\n\n\n")
        
        train_model(model, train_loader, criterion, optimizer, epoch,args.epoch_num,
                    args.gpu_batch_size, train_losses, used_criterion_th=used_criterion_th)
        evaluate(model, val_loader,  criterion, args.gpu_batch_size, val_losses)

        state = {
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
        }
        num = epoch
        while True:
            if os.path.isfile(os.path.join(args.checkpoint_path, "checkpoint_{}.pth".format(num))):
                num += 1
            else:
                break
        torch.save(state, os.path.join(
            args.checkpoint_path, "checkpoint_{}.pth".format(num)))

        stop_time = time.time()
        print("Epoch nr: ", epoch, "   |   Epoch time: ", stop_time - start_time)

        with open(os.path.join(args.checkpoint_path, "checkpoint_{}.pickle".format(num)), 'wb') as handle:
            to_dump = dict()
            to_dump['TrainLoss'] = train_losses
            to_dump['ValLoss'] = val_losses
            pickle.dump(to_dump, handle)

        used_criterion_th = train_losses[-1]*args.criterion_percent

        if len(train_losses) >= 10:
            losses_vect = train_losses[len(train_losses)-10: len(train_losses)]
            min_elem = np.min(losses_vect)
            max_elem = np.max(losses_vect)
            mean_elem = np.mean(losses_vect)
            if np.abs(min_elem - mean_elem) < 0.01 and np.abs(max_elem - mean_elem) < 0.01:
                print("Train loss is constant. Reset ...")
                model.reset_network_weights()

        # compare_trajectory(os.path.join(args.checkpoint_path, "checkpoint_{}".format(
        #     num)), model, args.test_datapath, 1)

        plt.figure(1)
        plt.plot(np.arange(1, len(train_losses) + 1), train_losses)
        plt.plot(np.arange(1, len(train_losses) + 1), val_losses)
        plt.title("Network training history")
        plt.legend(['Train LOSS', 'Validation LOSS'])
        plt.savefig(os.path.join(args.checkpoint_path, "learning_process.png"), dpi=300)
        plt.close('all')
        

    print("\n\n")
    print("----------------------------------------------------------------------------------------------------")
    print("Network train success!")
    plt.figure(1)
    plt.plot(np.arange(1,args.epoch_num + 1), train_losses)
    plt.plot(np.arange(1,args.epoch_num + 1), val_losses)
    plt.title("Network training history")
    plt.legend(['Train LOSS', 'Validation LOSS'])
    plt.savefig(os.path.join(args.checkpoint_path, "learning_process.png"), dpi=300)
    plt.show()


def evaluate_model(model, val_loader, criterion, batch_size, val_losses, dtype=FLOAT):
    epoch_val_losses = list()
    model.eval()
    with torch.no_grad():
        print('\n\n\n')
        for batch_idx, (images_stacked, odometries_stacked) in enumerate(val_loader):

            if USE_CUDA:
                images_stacked = images_stacked.cuda()
                odometries_stacked = odometries_stacked.cuda()
            images_stacked = Variable(images_stacked).type(dtype)
            odometries_stacked = Variable(odometries_stacked).type(dtype)

            estimated_odometries = Variable(
                torch.zeros(odometries_stacked.shape)).type(dtype)
            if USE_CUDA:
                estimated_odometries = estimated_odometries.cuda()

            for t in range(batch_size):
                model.reset_hidden_states(size=TRAJECTORY_LENGTH, zero=True)
                estimated_odometry = model(images_stacked[t])
                estimated_odometries[t] = estimated_odometry

            val_loss = criterion(estimated_odometries, odometries_stacked)

            epoch_val_losses.append(np.float(val_loss.data.cpu()))
        print("Mean validation LOSS for epoch: ", np.mean(epoch_val_losses),)
        val_losses.append(np.mean(epoch_val_losses))


def compare_trajectory(save_as, model, test_datapath, batch_size, dtype=FLOAT):

    paths = []
    for file in os.listdir(test_datapath):
        if file.endswith(".pickle"):
            paths.append(test_datapath+file)

    test_num = 0
    for path_to_pickle in paths:
        print('\n\n')
        images = list()
        poses = list()
        images_stacked = []
        odometries = []

        test_num += 1
        if os.path.isfile(path_to_pickle):
            with open(path_to_pickle, 'rb') as handle:
                dataset = pickle.load(handle)
                for i in range(len(dataset['img'])):
                    x = dataset['north'][i] * SCALE_ODOM
                    y = dataset['east'][i] * SCALE_ODOM
                    yaw = np.deg2rad(
                        dataset['yaw'][i]-dataset['yaw'][0]) * SCALE_ODOM
                    odom = np.array([x, y, yaw]).astype(np.float32)
                    poses.append(odom)
                    images.append(np.array(dataset['img'][i]).astype(
                        np.float32) / (2.0**8-1))
                dataset.clear()
        print("Data loaded. Processing...")

        images_stacked = np.asarray(images_stacked)
        odometries = np.asarray(odometries)
        model.training = False
        epoch_val_losses = list()
        model.eval()
        with torch.no_grad():
            real = []
            estim = []
            real.append(np.zeros(3))
            estim.append(np.zeros(3))

            # for i in range(199):
            for i in tqdm(range(len(images)-2)):
                img1 = images[i]
                img2 = images[i+1]
                pose1 = poses[i]
                pose2 = poses[i+1]
                odom = pose2 - pose1
                img1 = img1.reshape(720, 1280, 1)
                img2 = img2.reshape(720, 1280, 1)
                img = np.concatenate([img1, img2], axis=2)
                img = np.transpose(img, [2, 0, 1])

                tmp = []
                tmp.append(odom)
                odom = torch.Tensor(tmp)
                tmp = []
                tmp.append(img)
                image = torch.Tensor(tmp)
                image = Variable(image).type(dtype)
                odom = Variable(odom).type(dtype)
                estimated_odom = Variable(torch.zeros(odom.shape)).type(dtype)

                if USE_CUDA:
                    image = image.cuda()
                    odom = odom.cuda()
                    estimated_odom = estimated_odom.cuda()

                estimated_odometry = model(image)

                odom = odom.cpu().numpy()
                estimated_odometry = estimated_odometry.cpu().numpy()
                real.append(real[-1]+odom[0])
                estim.append(np.asarray(estim[-1]) +
                             np.asarray(estimated_odometry[0]))
        real = np.asarray(real)
        estim = np.asarray(estim)
        with open(save_as+'_'+str(test_num)+'_estimated_odometry.pickle', 'wb') as file:
            pickle.dump(estim, file)

        dx = estim[:, 0]/SCALE_ODOM
        dy = estim[:, 1]/SCALE_ODOM
        dth = estim[:, 2]/SCALE_ODOM
        reversed_x = np.zeros((len(dx)), np.float64)
        reversed_y = np.zeros((len(dx)), np.float64)
        reversed_yaw = np.zeros((len(dx)), np.float64)
        system_coords = np.array([0, 0, 0])
        for i in range(len(dx) - 1):
            local_delta = np.array([dx[i], dy[i]])
            global_detla = rotate([0, 0], local_delta, system_coords[2])

            reversed_x[i+1] = system_coords[0] + global_detla[0]
            reversed_y[i+1] = system_coords[1] + global_detla[1]
            reversed_yaw[i+1] = system_coords[2] + dth[i]
            system_coords = np.array(
                [reversed_x[i+1], reversed_y[i+1], reversed_yaw[i+1]])

        # print(estim.shape)
        plt.figure(2)
        plt.clf()
        plt.plot(real[:, 0]/SCALE_ODOM,
                 real[:, 1]/SCALE_ODOM)
        plt.plot(reversed_x,
                 reversed_y)
        plt.legend(['orginal', 'estimation'])
        plt.xlabel('[m]')
        plt.ylabel('[m]')
        # plt.show()
        plt.savefig(save_as+'_'+str(test_num)+'.png', dpi=200)


def evaluate(model, val_loader, criterion, batch_size,  val_losses):
    model.training = False

    kwargs = {'num_workers': 1, 'pin_memory': True} if torch.cuda.is_available() else {
    }

    evaluate_model(model, val_loader, criterion, batch_size, val_losses)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='PyTorch on Place Recognition + Visual Odometry')

    parser.add_argument('--gpu_batch_size', default=1,
                        type=int, help='gpu batch size')
    parser.add_argument('--lr', type=float, default=0.001)

    parser.add_argument('--momentum', type=float, default=0.1)

    parser.add_argument('--criterion_percent', type=float, default=0.1)
    parser.add_argument('--epoch_num', default=100,
                        type=int, help='epoch number')

    parser.add_argument('--val_datapath', default='/home/atlas/DeepVO_Dataset_RGB/TestOne/', type=str,
                        help='path to_validation dataset')
    parser.add_argument('--train_datapath', default='/home/atlas/DeepVO_Dataset_RGB/Train/', type=str,
                        help='path to train dataset')
    parser.add_argument('--test_datapath', default='/home/atlas/DeepVO_Dataset_RGB/TestOne/', type=str,
                        help='path to test dataset')
    parser.add_argument('--checkpoint_path', default="/home/atlas/DeepVO_Dataset_RGB/Checkpoint/", type=str,
                        help='Checkpoint path')
    # parser.add_argument('--checkpoint', default="/home/atlas/DeepVO_Dataset_RGB/Checkpoint/checkpoint_9.pth", type=str)
    parser.add_argument('--checkpoint', default=None, type=str)

    args = parser.parse_args()

    print("Arguments parsed.")

    model = DeepOdomNet(trajectory_length=TRAJECTORY_LENGTH)

    if args.checkpoint is not None:
        print('Statring training from checkpoint: ', args.checkpoint)
        checkpoint = torch.load(args.checkpoint)
        model.load_state_dict(checkpoint['state_dict'], strict=False)
    if USE_CUDA:
        model.cuda()

    args = parser.parse_args()
    train(model, args)
#     compare_trajectory("/home/atlas/DeepVO_Dataset_RGB/Checkpoint/_test",model, args.test_datapath, 1)

    