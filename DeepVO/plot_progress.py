import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
plt.switch_backend('TkAgg')

path_to_pickle = '/home/atlas/DeepVO_Dataset_RGB/Checkpoint/checkpoint_26.pickle'
print("Checkpoint pickle loading: Path: ", path_to_pickle)

if os.path.isfile(path_to_pickle):
    with open(path_to_pickle, 'rb') as handle:
        losses = pickle.load(handle)

plt.figure()
plt.plot(losses['TrainLoss'])
plt.plot(losses['ValLoss'])
plt.title("Learning process - Train and Validation losses")
plt.legend(['Train', 'Validation'])
plt.show()
