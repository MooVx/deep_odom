# /usr/bin/python3
import glob
import math
import pickle
from math import sqrt, pi, sin, cos, tan
import matplotlib.pyplot as plt

import numpy as np
import rosbag
from scipy import signal
import os
import cv2

class GpsPostProcess:

    def __init__(self):
        self.yaw_prev = 0
        self.east_temp = []
        self.north_temp = []
        self.e_start = 0
        self.n_start = 0
        self.yaw = 0.0
        self.quaternion = np.array([0, 0, 0, 1])

    def process_bag(self, bag_name, topic_name, only_fix=False, filtering=True):
        data = {'latitude': list(),
                'longitude': list(),
                'time': list(),
                'north': list(),
                'east': list(),
                'yaw': list()}
        with rosbag.Bag(bag_name) as bag:
            total_iter = bag.get_message_count(topic_filters=[topic_name])
            printProgressBar(0, total_iter, prefix='    Processing GPS:  ')
            iter = 0
            for topic, msg, t in bag.read_messages(topics=[topic_name]):
                iter += 1
                printProgressBar(iter, total_iter, prefix='    Processing GPS:  ')
                if (only_fix == False) or (msg.status.status == 2):
                    north, east = to_pl2000(msg)
                    if (not np.isnan(north)) and (not np.isnan(east)):
                        yaw = self.estimate_angle(north, east)
                        data['latitude'].append(msg.latitude)
                        data['longitude'].append(msg.longitude)
                        data['time'].append(msg.header.stamp.to_time())
                        data['north'].append(north)
                        data['east'].append(east)
                        data['yaw'].append(yaw)
                else:
                    data['latitude'].append(np.nan)
                    data['longitude'].append(np.nan)
                    data['time'].append(msg.header.stamp.to_time())
                    data['north'].append(np.nan)
                    data['east'].append(np.nan)
                    data['yaw'].append(np.nan)

        for i in range(len(data['yaw'])):
            if data['yaw'][i] == 0.0 and data['yaw'][i + 1] != 0.0:
                for j in range(i + 1):
                    data['yaw'][j] = float(data['yaw'][i + 1])
                break

        if filtering:
            data['north'] = signal.savgol_filter(signal.medfilt(data['north'], 5), 31, 3)
            data['east'] = signal.savgol_filter(signal.medfilt(data['east'], 5), 31, 3)
            data['yaw'] = signal.savgol_filter(signal.medfilt(data['yaw'], 7), 41, 3)

        return data

    def estimate_angle(self, north, east):
        min_delta = 0.1
        max_delta = 20.0
        N = len(self.east_temp)

        if N == 0:
            self.e_start = east
            self.n_start = north

        if N > 6:
            self.east_temp.pop(0)
            self.north_temp.pop(0)
        self.east_temp.append(east - self.e_start)
        self.north_temp.append(north - self.n_start)

        if N >= 2:
            # Dystans przejechany przez EVE liczony z ostatnich n probek
            vect_lenght = np.sqrt(pow((self.north_temp[N - 1] - self.north_temp[0]), 2)
                                  + pow((self.east_temp[N - 1] - self.east_temp[0]), 2))
            # Sprawdzamy czy EVE przejechala minimalny dystans i czy nie wystapil
            # blad gruby - jesli tak - nie wyliczamy kata.
            if vect_lenght >= min_delta and vect_lenght <= max_delta:
                x = np.array(self.north_temp)
                y = np.array(self.east_temp)
                # wyznaczenie wspolczynnika nachylenia prostej a metoda regresji liniowej
                # kat jest estymowany na podstawie n probek
                A = np.vstack([x, np.ones(len(x))]).T
                a, b = np.linalg.lstsq(A, y, rcond=-1)[0]
                angle = np.rad2deg(np.arctan(a))
                angle = np.rad2deg(np.arctan(a))
                # I cwiartka
                if ((self.north_temp[0] < self.north_temp[-1]) and (self.east_temp[0] < self.east_temp[-1]) and (
                        0 < angle) and (angle < 90)):
                    yaw = angle
                # IV cwiartka
                elif ((self.north_temp[0] < self.north_temp[-1]) and (self.east_temp[0] > self.east_temp[-1]) and (
                        -90 < angle) and (angle < 0)):
                    yaw = angle + 360
                # III cwiartka
                elif ((self.north_temp[0] > self.north_temp[-1]) and (self.east_temp[0] > self.east_temp[-1]) and (
                        0 < angle) and (angle < 90)):
                    yaw = angle + 180
                # II cwiartka
                elif ((self.north_temp[0] > self.north_temp[-1]) and (self.east_temp[0] < self.east_temp[-1]) and (
                        -90 < angle) and (angle < 0)):
                    yaw = angle + 180
                else:
                    yaw = self.yaw_prev
            else:
                yaw = self.yaw_prev
        else:
            yaw = self.yaw_prev

        if self.yaw_prev > 300.0 and yaw < 60.0:
            self.yaw += 360.0
        elif yaw > 300.0 and self.yaw_prev < 60.0:
            self.yaw -= 360.0

        self.yaw += yaw - self.yaw_prev
        self.yaw_prev = yaw
        return self.yaw


def to_pl2000(fix_msg):
    a = 6378137.0000  # m
    b = 6356752.3141  # m
    L0 = 21.000000000000000000  # degrees
    B1 = fix_msg.latitude  # degrees
    L1 = fix_msg.longitude  # degrees
    e1 = sqrt((a ** 2 - b ** 2) / (a ** 2))
    e2 = sqrt((a ** 2 - b ** 2) / (b ** 2))
    B1 = B1 * pi / 180  # rad
    N1 = a / sqrt(1 - e1 ** 2 * (sin(B1)) ** 2)
    SWGS84 = 6367449.14577 * B1 - 16038.50874 * sin(2 * B1) + 16.83261 * sin(4 * B1) - 0.02198 * sin(
        6 * B1) + 0.00003 * sin(8 * B1)
    L0 = L0 * pi / 180  # rad
    L1 = L1 * pi / 180  # rad
    l = L1 - L0
    t = tan(B1)
    eta = e2 * cos(B1)
    xgk = SWGS84 + l ** 2 / 2 * N1 * sin(B1) * cos(B1) + (l ** 4 / 24 * N1 * sin(B1) * (cos(B1)) ** 3) * (
            5 - t ** 2 + 9 * eta ** 2 + 4 * eta ** 4) + l ** 6 / 720 * N1 * sin(B1) * (cos(B1)) ** 5 * (
                  61 - 58 * t ** 2 + t ** 4)
    ygk = l * N1 * cos(B1) + l ** 3 / 6 * N1 * (cos(B1)) ** 3 * (1 - t ** 2 + eta ** 2) + l ** 5 / 120 * N1 * (
        cos(B1)) ** 5 * (5 - 18 * t ** 2 + t ** 4 + 14 * eta ** 2 - (58 * eta) ** 2 * t ** 2)
    east = 0.999923 * xgk
    north = 7 * 1000000 + 500000 + ygk * 0.999923
    return north, east


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=50, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def rotate(x_vect, y_vect, angle):
    angle = np.deg2rad(angle)
    out_x = np.zeros(x_vect.shape)
    out_y = np.zeros(y_vect.shape)
    for i in range(len(x_vect)):
        out_x[i] = math.cos(angle) * x_vect[i] - math.sin(angle) * y_vect[i]
        out_y[i] = math.sin(angle) * x_vect[i] + math.cos(angle) * y_vect[i]
    return out_x, out_y

# import cv2
def process_bags(bag_folder, out_folder, depth_topic='/realsense/image_depth', split_size=500, min_dist=1.0,split_start_num=0):
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)

    split = split_start_num
    for bag_name in glob.glob(bag_folder + "/*.bag"):
        print('\n' + bag_name)
        gps = GpsPostProcess()
        gps_data = gps.process_bag(bag_name, '/leica_gps/fix', filtering=True)

        with rosbag.Bag(bag_name) as bag:
            prev_valid = True
            dataset = {'img': list(),
                       'north': list(),
                       'east': list(),
                       'yaw': list()}
            total_iter = bag.get_message_count(topic_filters=[depth_topic])
            iterValid = 0
            if total_iter > 0:
                printProgressBar(0, total_iter, prefix='    Processing Depth:')
                iter = 0
                for topic, msg, t in bag.read_messages(topics=[depth_topic]):
                    iter += 1
                    printProgressBar(iter, total_iter, prefix='    Processing Depth:',
                                     suffix=f' {len(dataset["img"])}/{split_size} ')
                    index = np.argmin(np.abs(np.array(gps_data['time']) - msg.header.stamp.to_time()))
                    if gps_data['time'][index] > msg.header.stamp.to_time():
                        index -= 1
                    if index + 1 < len(gps_data['time']) and index > 1:
                        if np.isnan(gps_data['north'][index]) or np.isnan(gps_data['north'][index + 1]) or \
                                np.isnan(gps_data['east'][index + 1]) or np.isnan(gps_data['east'][index + 1]):
                            if prev_valid:
                                iterValid -= len(dataset['img'])
                                dataset = {
                                    'img': list(),
                                           'north': list(),
                                           'east': list(),
                                           'yaw': list()}
                                split += 1
                            prev_valid = False
                        else:
                            if len(dataset['img']) > split_size:
                                printProgressBar(iter, total_iter, prefix=f'    Saving Split  {split} :',
                                                 suffix=f' {len(dataset["img"]) - 1}/{split_size}    ')
                                if np.sqrt((max(dataset['north']) - min(dataset['north'])) ** 2 + (
                                        max(dataset['east']) - min(dataset['east'])) ** 2) > min_dist:
                                    dataset['north'] -= dataset['north'][0]
                                    dataset['east'] -= dataset['east'][0]
                                    dataset['north'], dataset['east'] = rotate(dataset['north'], dataset['east'],
                                                                               -np.mean(dataset['yaw'][0:10]))
                                    plt.ioff()
                                    fig = plt.figure()
                                    plt.title(f'dataset_{split:3d}')
                                    plt.scatter(dataset['north'], dataset['east'],0.5)
                                    fig.axes[0].axis('equal')
                                    plt.savefig(f"{out_folder}/dataset_{split:03d}.png")
                                    plt.ion()
                                    plt.close(fig)
                                    with open(f"{out_folder}/dataset_{split:03d}.pickle", 'wb') as file:
                                        pickle.dump(dataset, file)
                                    split += 1
                                else:
                                    iterValid -= len(dataset['img'])
                                dataset = {'img': list(),
                                           'north': list(),
                                           'east': list(),
                                           'yaw': list()}

                            time_window = gps_data['time'][index + 1] - gps_data['time'][index]
                            time_s = msg.header.stamp.to_time() - gps_data['time'][index]
                            dataset['north'].append(gps_data['north'][index] +
                                                    (gps_data['north'][index + 1] - gps_data['north'][index])
                                                    * (time_s / time_window))
                            dataset['east'].append(gps_data['east'][index] +
                                                   (gps_data['east'][index + 1] - gps_data['east'][index])
                                                   * (time_s / time_window))
                            dataset['yaw'].append(gps_data['yaw'][index] +
                                                  (gps_data['yaw'][index + 1] - gps_data['yaw'][index])
                                                  * (time_s / time_window))
                            
                            if msg.encoding == '16UC1':
                                img = np.frombuffer(msg.data, np.uint16).reshape((msg.height, msg.width))
                                dataset['img'].append(img)
                            if msg.encoding == 'rgb8':
                                img = np.frombuffer(msg.data, np.uint8).reshape((msg.height, msg.width,3))
                                dataset['img'].append(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY ))

                            iterValid += 1
                            prev_valid = True
                iterValid -= len(dataset["img"])
                dataset.clear()
                split += 1
            print(f'    Used {iterValid } frames of {iter}.')
    print(f'All of {split} Splits saved in: {out_folder}.')


if __name__ == "__main__":
    bagFolder = "/media/roman/Garry/2019_12_11/"
    outFolder = "/media/roman/Garry/2019_12_11/processed"


    process_bags(bagFolder,
                 outFolder,
                 depth_topic='/realsense/image_depth',
                 split_size=200,
                 min_dist=1.0)
    # bag_name = '/media/roman/Quick/2019_10_29/kolo_u2_b3_2019-10-29-11-47-04.bag'
    # gps = GpsPostProcess()
    # gps_data = gps.process_bag(bag_name, '/leica_gps/fix', filtering=True)
